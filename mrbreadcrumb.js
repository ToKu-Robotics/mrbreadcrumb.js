function setBreadcrumb(url) {
    if (url == undefined) {
        throw new Error('URL cannot be empty!')
        
    } else {
        let breadcrumbdata = url.split('/');
        let breadcrumbArray = [];
        let i;
        let entrybefore;
        for (i = 0; i < breadcrumbdata.length; i++) {
            let entry = "";
            if (i == 0) {
                entry = breadcrumbdata[i];
                if (entry == "") {
                    entry = {
                        url: "/",
                        name: "Home"
                    }
                    entrybefore = "";
                } else {
                    entry = {
                        url: breadcrumbdata[i],
                        name: "Home"
                    }
                    entrybefore = breadcrumbdata[i];
                }
            }
            if (i > 0) {
                entry = {
                    url: entrybefore + "/" + breadcrumbdata[i],
                    name: breadcrumbdata[i]
                }
                entrybefore = entrybefore + "/" + breadcrumbdata[i];
            }
            breadcrumbArray.push(entry);

        }
        return (breadcrumbArray)
    }
}

module.exports = setBreadcrumb;