**Mr. Breadcrumbs**

MrBreadcrumb.js returns an array of objects with URL and Name for your Breadcrumb.

So you can add easily a breadcrumb on your App.

**JS Example**

`const mrbreadcrumbs = require('mrbreadcrumbs.js');`
`let breadcrumbs = mrbreadcrumbs('projects/testproject/day1');`

Array will look like:

`[ { url: 'projects', name: 'Home' },`
` { url: 'projects/testproject', name: 'testproject' },`
` { url: 'projects/testproject/day1', name: 'day1' } ]`

**Example with Express**

`app.get('/YOURURL', function (req, res) {`
    `let breadcrumbs = mrbreadcrumb(req.originalUrl);`
    'console.log(breadcrumbs);'
`});`

